package z.com.walmartcodingassignment

import android.app.Application
import z.com.walmartcodingassignment.data.Repository
import z.com.walmartcodingassignment.data.network.FancyCarsApi
import z.com.walmartcodingassignment.data.network.ServiceGenerator

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Repository(
            ServiceGenerator.createService(
                FancyCarsApi::class.java,
                this
            )
        )
    }
}