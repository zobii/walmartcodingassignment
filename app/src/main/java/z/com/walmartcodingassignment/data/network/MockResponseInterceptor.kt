package z.com.walmartcodingassignment.data.network

import android.content.Context
import okhttp3.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*

class MockResponseInterceptor(private val context:Context) : Interceptor {
    private val MEDIA_JSON = MediaType.parse("application/json")

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val path = request.url().encodedPath()
        val stream = if (path.contains("cars"))
            context.assets.open("cars")
        else
            context.assets.open(if (Random().nextBoolean()) "available" else "unavailable")

        val json = parseStream(stream)

        return Response.Builder()
            .body(ResponseBody.create(MEDIA_JSON, json))
            .request(chain.request())
            .protocol(Protocol.HTTP_2)
            .code(200)
            .message("")
            .build()
    }

    @Throws(IOException::class)
    private fun parseStream(stream: InputStream): String {
        val builder = StringBuilder()
        val inStream = BufferedReader(InputStreamReader(stream, "UTF-8"))
        var line = inStream.readLine()
        while (line  != null) {
            builder.append(line)
            line = inStream.readLine()
        }
        inStream.close()
        return builder.toString()
    }
}