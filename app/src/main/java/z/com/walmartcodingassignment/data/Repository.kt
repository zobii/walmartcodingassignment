package z.com.walmartcodingassignment.data

import androidx.annotation.WorkerThread
import z.com.walmartcodingassignment.data.network.FancyCarsApi

class Repository(private val apiClient: FancyCarsApi) {

    companion object {
        lateinit var instance: Repository
            private set
    }

    init {
        instance = this
    }

    @WorkerThread
    suspend fun getCars() = apiClient.getCars().also {
            cars -> cars.forEach { it.available = getCarAvailability(it.id) }
    }

    @WorkerThread
    suspend fun getCarAvailability(carId:Int) =  apiClient.getCarAvailability(carId)
}