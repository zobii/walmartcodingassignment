package z.com.walmartcodingassignment.data.model

data class Car(
    var id: Int,
    var name: String,
    var make: String,
    var model: String,
    var year: String,
    var img: String,
    var available: Availability
)