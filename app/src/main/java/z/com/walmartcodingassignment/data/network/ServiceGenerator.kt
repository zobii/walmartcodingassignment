package z.com.walmartcodingassignment.data.network

import android.content.Context
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator {
    companion object {
        private const val BASE_URL = "https://www.fancycars.com"

        private val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        private var retrofit = builder.build()

        private val httpClient = OkHttpClient.Builder()

        fun <S> createService(serviceClass: Class<S>, context: Context): S {
            httpClient.addInterceptor(MockResponseInterceptor(context))
            builder.client(httpClient.build())
            retrofit = builder.build()

            return retrofit.create(serviceClass)
        }
    }
}