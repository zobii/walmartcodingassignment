package z.com.walmartcodingassignment.data.network

import retrofit2.http.GET
import retrofit2.http.Query
import z.com.walmartcodingassignment.data.model.Availability
import z.com.walmartcodingassignment.data.model.Car

interface FancyCarsApi {
    @GET("/cars")
    suspend fun getCars(): List<Car>

    @GET("/Availability")
    suspend fun getCarAvailability(@Query("id") carId: Int): Availability
}