package z.com.walmartcodingassignment.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.car_list_item.view.*
import z.com.walmartcodingassignment.R
import z.com.walmartcodingassignment.data.model.Car

class CarsAdapter: RecyclerView.Adapter<CarsAdapter.CarViewHolder>() {

    var carList = ArrayList<Car>()

    override fun getItemCount() = carList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        return CarViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.car_list_item,
                parent,
                false
            )
        )
    }

    fun addCars(cars:List<Car>){
        carList.addAll(cars)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) = holder.bind(carList[position])

    fun sortByCarName() {
        carList.sortWith(Comparator { car1, car2 -> car1.name.compareTo(car2.name)})
        notifyDataSetChanged()
    }

    fun sortByAvailability() {
        carList.sortWith(Comparator { car1, car2 ->
            return@Comparator if (car1.available.available == "In Dealership" && car2.available.available != "In Dealership") {
                -1
            } else if (car1.available.available != "In Dealership" && car2.available.available == "In Dealership") {
                1
            } else
                0
        })
        notifyDataSetChanged()
    }

    class CarViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        fun bind(car: Car) {
            with(itemView){
                Picasso.get().load(car.img).into(image)
                name.text = car.name
                make.text = car.make
                model.text = car.model
                year.text = car.year
                if(car.available.available != "null"){
                    avaliable.text = car.available.available
                    buy.visibility = if (car.available.available.equals("In Dealership", true)) View.VISIBLE else View.GONE
                }
            }
        }
    }

}