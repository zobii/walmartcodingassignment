package z.com.walmartcodingassignment.ui.viewmodel

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import z.com.walmartcodingassignment.data.Repository
import z.com.walmartcodingassignment.data.model.Car
import z.com.walmartcodingassignment.data.network.Result
import java.lang.Exception

class MainViewModel(private val repository: Repository): ViewModel() {

    private val mutableCarsList : MutableLiveData<Result<List<Car>>> = MutableLiveData()

    val carsList:LiveData<Result<List<Car>>> = mutableCarsList

    fun fetchCars(){
        viewModelScope.launch {
            mutableCarsList.postValue(Result.Loading)
            try {
                mutableCarsList.postValue(Result.Success(repository.getCars()))
            } catch (e: Exception) {
                mutableCarsList.postValue(Result.Error(e))
            }
        }
    }
}