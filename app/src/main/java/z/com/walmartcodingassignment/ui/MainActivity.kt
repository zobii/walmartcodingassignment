package z.com.walmartcodingassignment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import z.com.walmartcodingassignment.ui.viewmodel.MainViewModel
import z.com.walmartcodingassignment.ui.viewmodel.MainViewModelFactory
import z.com.walmartcodingassignment.R
import z.com.walmartcodingassignment.data.Repository
import z.com.walmartcodingassignment.data.model.Car
import z.com.walmartcodingassignment.data.network.FancyCarsApi
import z.com.walmartcodingassignment.data.network.Result
import z.com.walmartcodingassignment.data.network.ServiceGenerator

class MainActivity : AppCompatActivity() {

    lateinit var mainVM: MainViewModel
    lateinit var carsListAdapter: CarsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        carsList.layoutManager = LinearLayoutManager(this)
        carsList.adapter = CarsAdapter().also {
            carsListAdapter = it
        }
        carsList.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                takeUnless { recyclerView.canScrollVertically(1) }?.let {
                    mainVM.fetchCars()
                }
            }
        })
        mainVM = ViewModelProvider(this, MainViewModelFactory(Repository.instance))
            .get(MainViewModel::class.java)

        mainVM.fetchCars()
        mainVM.carsList.observe(this, Observer {
            when(it){
                is Result.Loading -> { showLoader() }
                is Result.Success<List<Car>> -> {
                    hideLoader()
                    carsListAdapter.addCars(it.data)
                }
            }
        })
    }

    private fun hideLoader() {
        loader.visibility = View.GONE
    }

    private fun showLoader() {
        loader.visibility = View.VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.sort_name -> {
                carsListAdapter.sortByCarName()
                true
            }
            R.id.sort_available -> {
                carsListAdapter.sortByAvailability()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
